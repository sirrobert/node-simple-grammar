"use strict";

var Token = require('./src/Token.js');

class Grammar {

  constructor () {
    // A token lookup indexed by token.glyph
    this.tokens = {};
    this.strict = false;

    this.orderedTokens = '';
  }

  // Always returns an up-to-date regular expression representing all the
  // currently-available tokens.  Example:  /[WDPNIXR]/
  get tokenExpression () {
    var tokenExpression = '';

    for (var token in this.tokens) {
      tokenExpression += token;
    }

    return new RegExp('[' + tokenExpression + ']');
  }

  // Polymorphic:
  //
  //  * .addToken(glyph, value) { /* two strings */ }
  //  * .addToken(params) {/* an object with .glyph and .value values */}
  //
  addToken (glyph, value) {
    var params = {};
    if (glyph && !value && (typeof glyph === 'object')) {
      params = glyph;
    } else if (glyph && value) {
      params.glyph = glyph;
      params.value = value;
    }

    if (params.glyph in this.tokens) {
      throw new Error('Grammar already has token "' + params.glyph + '" with value "' + params.value + '"');
    }

    var token = new Token(params);

    this.tokens[params.glyph] = token
    return this;
  }

  // Given a format, recursively expand it until only a detokenized regexp
  // is left.  Returns the expanded form, NOT the regexp.  This is largely
  // so that a regexp can be built with flags, like 'g' if desired, or so
  // that qualifiers like ^ or $ can be inserted.
  expand (format) {
    var hasTokens = this.tokenExpression;

    while (format.match(hasTokens)) {
      for (var t in this.tokens) {
        format = format.replace(t, (t === 'W' && this.strict) ? '' : this.tokens[t]);
      }
    }

    return format;
  }

  // Take the bank of available tokens and order them by complexity.  By
  // default, this should be most to least, but you can reverse it by
  // passing in the "reverse" bool.
  //
  // Our specific measure of complexity is number of replacements required
  // to fully resolve the token as a pattern.
  tokensByComplexity (reverse) {
    reverse = !!reverse;

    var hasTokens = this.tokenExpression;

    var complexities = [];

    for (var str in this.tokens) {
      let original = str;
      let complexity = 0;

      // Just a note:  We don't need to accommodate whitespace here because
      // the "W" token has a complexity of 1.  By aborting the "W"
      // replacement, we don't reduce any complexity, and since the
      // replacement isn't used here, only the complexity measurement, we
      // can leave it out.
      while(str.match(hasTokens)) {
        for (var t in this.tokens) {
          if(str.match(t)) {
            complexity++
            str = str.replace(t, this.tokens[t]);
          }
        }
      }

      complexities.push([original, complexity]);
    }

    complexities.sort((a,b) => {
      return a[1] < b[1] ?  1
      :      a[1] > b[1] ? -1
      :                     0
    });

    return reverse ? complexities.reverse() : complexities;
  }


  // grammar.has(str, 'I') checks if the provided string has an integer
  // pattern in it.
  has (str, format) {
    var format = this.expand(format);
    var regexp = new RegExp(format);

    return !!str.trim().match(regexp);
  }

  // grammar.has(str, 'I') checks if the provided string *is* an integer
  // pattern.  It can't have anything else.  The string is trimmed first, so
  // whitespace before or after are ignored.
  is (str, format) {
    var orig = format;
    var format = '^' + this.expand(format) + '$';
    var regexp = new RegExp(format);

    return !!(str || '').trim().match(regexp);
  }

  count (str, format) {
    var format = this.expand(format);
    var regexp = new RegExp(format, 'g');

    var matches = str.trim().match(regexp);
    return matches ? matches.length : 0;
  }

  // grammar.find(str, 'I').  Find the pattern in the string.
  find (str, format) {
    var format = this.expand(format);
    var regexp = new RegExp(format, 'g');

    if (this.orderedTokens) {
      var ordered = str.trim().match(
        new RegExp(this.expand(this.orderedTokens), 'g')
      );
    }

    var matches = [];
    ordered.forEach(n => {
      if (this.is(n, format)) {
        matches.push(n.trim());
      }
    });

    return matches;
  }

  // Takes a string and tries to identify what kind of number it is based on
  // the available patterns.  Returns the grammar token, not a name _per se_.
  identify (str, aggregateForms) {

    if (typeof str === 'undefined' || str === null) {
      return NaN;
    }

    var matches = [];
    var tokenList = this.tokensByComplexity();

    tokenList.forEach(([token,complexity]) => {
      if (this.is(str,token)) {
        matches.push(token)
      }
    });

    return matches[0] || NaN;
  }

}

module.exports = Grammar;

