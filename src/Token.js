"use strict";

class Token {

  constructor (params={}) {
    if (typeof params.glyph !== 'undefined') {
      this.glyph = params.glyph;
      this.value = params.glyph;
    }

    if (typeof params.value !== 'undefined') {
      this.value = params.value;
    }

    if (params.isTerminal === true) {
      this.value = this.glyph;
    }
  }

  get glyph () {
    return this._glyph;
  }

  set glyph (val) {
    this._glyph = '' + val;
  }

  get value () {
    return this._value;
  }

  set value (val) {
    this._value = val;
  }

  get isVariable () {
    return !this.isTerminal;
  }

  set isTerminal (val) {
    if (val) {
      this.value = this.glyph;
    }
  }

  get isTerminal () {
    if (this.glyph === this.value) {
      return true;
    }
    return false;
  }

  get isComposite () {
    return !!this._isComposite;
  }

  set isComposite (val) {
    if (!!val) {
      this._isComposite = true;
    } else {
      this._isComposite = false;
    }
  }

  toJSONObject () {
    return {
      glyph       : this.glyph,
      value       : this.value,
      isTerminal  : this.isTerminal,
      isComposite : this.isComposite,
    };
  }
}

module.exports = Token;
